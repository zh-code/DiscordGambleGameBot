﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using DiscordGambleGameBot.Models;
using DiscordGambleGameBot.Modules;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reflection;
using System.Threading.Tasks;

namespace DiscordGambleGameBot
{
    class Program
    {
        static void Main(string[] args)
        {
            using (GambleContext _context = new GambleContext())
            {
                _context.Database.EnsureCreated();
            }
            new Program().RunBotAsync().GetAwaiter().GetResult();
        } 

        private DiscordSocketClient _client;
        private CommandService _commands;
        private IServiceProvider _services;

        public async Task RunBotAsync()
        {
            _client = new DiscordSocketClient();
            _commands = new CommandService();

            _services = new ServiceCollection()
                .AddSingleton(_client)
                .AddSingleton(_commands)
                .BuildServiceProvider();

            string botPrefix = Environment.GetEnvironmentVariable("BOT_TOKEN");

            //event subscriptions
            _client.Log += Log;
            //_client.Disconnected += DisconnectedAsync;
            //_client.UserJoined += AnnounceUserJoined;

            await RegisterCommandsAsync();

            await _client.LoginAsync(TokenType.Bot, botPrefix);

            await _client.StartAsync();

            await Task.Delay(-1);
        }


        private Task Log(LogMessage arg)
        {
            Console.WriteLine(arg);

            return Task.CompletedTask;
        }

        public async Task RegisterCommandsAsync()
        {
            _client.MessageReceived += HandleCommandAsync;
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly());
        }

        private async Task HandleCommandAsync(SocketMessage arg)
        {
            SocketUserMessage message = arg as SocketUserMessage;

            if (message == null || message.Author.IsBot || message.Author.IsWebhook) return;

            int argPos = 0;

            if (message.HasStringPrefix("~bet ", ref argPos) || message.HasMentionPrefix(_client.CurrentUser, ref argPos))
            {
                SocketCommandContext context = new SocketCommandContext(_client, message);
                HandleCommandThreadAsync(context, argPos, _services).Start();
            }
        }

        private async Task HandleCommandThreadAsync(SocketCommandContext context, int argPos, IServiceProvider _services)
        {
            var result = await _commands.ExecuteAsync(context, argPos, _services);
            if (!result.IsSuccess)
            {
                await new HILO().ShowHelp();
            }
        }
    }
}
