﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DiscordGambleGameBot.Models
{
    public class BalanceChangeLogModel : DbContext
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public int OriginalBalance { get; set; }

        [Required]
        public int ChangeAmount { get; set; }

        [Required]
        public int UpdatedBalance { get; set; }
    }
}
