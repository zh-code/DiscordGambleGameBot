﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DiscordGambleGameBot.Models
{
    public class UserModel : DbContext
    {
        [Key]
        public ulong UserId { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        public int Balance { get; set; }

        [Required]
        public DateTime NextFreeCoinDate { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
        
        public virtual ICollection<BetLogModel> BetLogs { get; set; }
    }
}
