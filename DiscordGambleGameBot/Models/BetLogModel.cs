﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DiscordGambleGameBot.Models
{
    public class BetLogModel : DbContext
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public GameEnum Game { get; set; }

        [Required]
        public BetInfoEnum Bet { get; set; }

        [Required]
        public int BetAmount { get; set; }

        [Required]
        public ulong UserId { get; set; }

        [Required]
        public UserModel User { get; set; }

        [Required]
        public int BalanceChangeLogId { get; set; }
        [Required]
        public BalanceChangeLogModel BalanceChangeLog { get; set; }
    }
}
