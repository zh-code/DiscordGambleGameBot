﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DiscordGambleGameBot.Models
{
    public class GambleContext : DbContext
    {
        public DbSet<UserModel> UserTable { get; set; }
        public DbSet<BetLogModel> BetLogTable { get; set; }
        public DbSet<BalanceChangeLogModel> BalanceChangeTable { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=db;Database=Gamble;Integrated Security=False;user ID=sa;Password=" + Environment.GetEnvironmentVariable("DATABASE_PASSWORD"));
        }
    }
}
