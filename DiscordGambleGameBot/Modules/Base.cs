﻿using Discord;
using Discord.Commands;
using DiscordGambleGameBot.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DiscordGambleGameBot.Modules
{
    public class Base : ModuleBase<SocketCommandContext>
    {
        protected GambleContext _context = new GambleContext();

        protected static double cut = 0.5;

        protected static int freeCoinAmount = 100;

        private static readonly Random _global = new Random();
        protected Random _local;

        public Base()
        {
            _local = new Random(_global.Next());
        }

        protected EmbedBuilder builder = new EmbedBuilder
        {
            Footer = new EmbedFooterBuilder()
            {
                Text = "Gamble Bot",
                IconUrl = "https://cdn4.iconfinder.com/data/icons/casino-glyph/500/casino-Glyph-13-512.png"
            },
            Timestamp = new DateTimeOffset(DateTime.UtcNow)
        };

        protected double Roll()
        {
            int rInt = _local.Next(0, 100);
            return Math.Round(_local.NextDouble() * 100, 2);
        }
    }
}
