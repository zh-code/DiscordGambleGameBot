﻿using Discord;
using Discord.Commands;
using DiscordGambleGameBot.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordGambleGameBot.Modules
{
    public class HILO : Base
    {
        [Command("Freecoin")]
        public async Task GetFreeCoin()
        {
            UserModel user = await _context.UserTable.FindAsync(Context.User.Id);
            if (user == null)
            {
                user = new UserModel
                {
                    UserId = Context.User.Id,
                    NextFreeCoinDate = new DateTime(1999, 1, 1),
                    Balance = 0,
                    Username = Context.User.Username
                };
                _context.UserTable.Add(user);
                await _context.SaveChangesAsync();
            }

            while (true)
            {
                builder.Fields.Clear();
                try
                {
                    if (DateTime.UtcNow.Subtract(user.NextFreeCoinDate).TotalHours >= 1)
                    {
                        user.Balance += freeCoinAmount;
                        user.NextFreeCoinDate = DateTime.UtcNow.AddHours(1);

                        await _context.SaveChangesAsync();

                        builder.AddField("Get free coins", freeCoinAmount.ToString() + " coins has been added to your account").WithColor(Color.Blue);
                    }
                    else
                    {
                        builder.AddField("Get free coins", "Please wait for another " + 
                            ((int)user.NextFreeCoinDate.Subtract(DateTime.UtcNow).TotalMinutes).ToString() + " minutes")
                            .WithColor(Color.Blue);
                    }
                    break;
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    ex.Entries.ToList().ForEach(x => x.Reload());
                }
            }
            await ReplyAsync($"{Context.User.Mention}", false, builder.Build());
        }

        [Command("Hi")]
        public async Task BetHi(int amount)
        {
            UserModel user = await _context.UserTable.FindAsync(Context.User.Id);

            if (user == null)
            {
                builder.AddField("Bet Result", "No player found. Please claim free coins first").WithColor(Color.Red);
            }
            else if (user.Balance <= 0 || user.Balance < amount)
            {
                builder.AddField("Bet Result", "Not enough coins left. You can try to claim free coins.").WithColor(Color.Red);
            }
            else
            {
                int previousBalance = user.Balance;
                bool win = false;
                double roll = Roll();
                while (true)
                {
                    try
                    {
                        if (roll > 50 + cut)
                        {
                            user.Balance += amount;
                            win = true;
                        }
                        else
                        {
                            user.Balance -= amount;
                            win = false;
                        }
                        await _context.SaveChangesAsync();
                        break;
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        ex.Entries.ToList().ForEach(x => x.Reload());
                    }
                }

                BetLogModel log = new BetLogModel
                {
                    Date = DateTime.UtcNow,
                    Bet = BetInfoEnum.Hi,
                    BetAmount = amount,
                    Game = GameEnum.HILO,
                    User = user,
                    UserId = user.UserId,
                    BalanceChangeLog = new BalanceChangeLogModel
                    {
                        ChangeAmount = amount,
                        OriginalBalance = previousBalance,
                        UpdatedBalance = user.Balance
                    }
                };
                _context.BetLogTable.Add(log);
                await _context.SaveChangesAsync();

                if (win)
                {
                    builder.AddField("Bet Result", "Bet: Hi\nAmount: " + amount + "\nRoll number: " + roll.ToString() +
                        "\n\nYou won " + amount.ToString() + " coins!!!").WithColor(Color.Blue);
                }
                else
                {
                    builder.AddField("Bet Result", "Bet: Hi\nAmount: " + amount + "\nRoll number: " + roll.ToString() +
                        "\n\nYou lost...").WithColor(Color.Gold);
                }

            }
            await ReplyAsync($"{Context.User.Mention}", false, builder.Build());
        }

        [Command("Lo")]
        public async Task BetLo(int amount)
        {
            UserModel user = await _context.UserTable.FindAsync(Context.User.Id);

            if (user == null)
            {
                builder.AddField("Bet Result", "No player found. Please claim free coins first").WithColor(Color.Red);
            }
            else if (user.Balance <= 0 || user.Balance < amount)
            {
                builder.AddField("Bet Result", "Not enough coins left. You can try to claim free coins.").WithColor(Color.Red);
            }
            else
            {
                int previousBalance = user.Balance;
                bool win = false;
                double roll = Roll();
                while (true)
                {
                    try
                    {
                        if (roll < 50 - cut)
                        {
                            user.Balance += amount;
                            win = true;
                        }
                        else
                        {
                            user.Balance -= amount;
                            win = false;
                        }
                        await _context.SaveChangesAsync();
                        break;
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        ex.Entries.ToList().ForEach(x => x.Reload());
                    }
                }

                BetLogModel log = new BetLogModel
                {
                    Date = DateTime.UtcNow,
                    Bet = BetInfoEnum.Lo,
                    BetAmount = amount,
                    Game = GameEnum.HILO,
                    User = user,
                    UserId = user.UserId,
                    BalanceChangeLog = new BalanceChangeLogModel
                    {
                        ChangeAmount = amount,
                        OriginalBalance = previousBalance,
                        UpdatedBalance = user.Balance
                    }
                };
                _context.BetLogTable.Add(log);
                await _context.SaveChangesAsync();

                if (win)
                {
                    builder.AddField("Bet Result", "Bet: Lo\nAmount: " + amount + "\nRoll number: " + roll.ToString() +
                        "\n\nYou won " + amount.ToString() + " coins!!!").WithColor(Color.Blue);
                }
                else
                {
                    builder.AddField("Bet Result", "Bet: Lo\nAmount: " + amount + "\nRoll number: " + roll.ToString() +
                        "\n\nYou lost...").WithColor(Color.Gold);
                }

            }
            await ReplyAsync($"{Context.User.Mention}", false, builder.Build());
        }

        [Command("Balance")]
        public async Task Getbalance()
        {
            UserModel user = await _context.UserTable.FindAsync(Context.User.Id);

            if (user == null)
            {
                builder.AddField("Balance", "No player found. Please claim free coins first").WithColor(Color.Red);
            }
            else
            {
                builder.AddField("Balance", "There are " + user.Balance.ToString() + " coins left in your account.").WithColor(Color.Blue);
            }
            await ReplyAsync($"{Context.User.Mention}", false, builder.Build());
        }

        [Command("Top5")]
        public async Task GetTop5()
        {
            List<UserModel> userList = _context.UserTable.OrderByDescending(x => x.Balance).Take(5).ToList();
            for (int i = 0; i < userList.Count; i++)
            {
                builder.AddField("Rank " + (i + 1).ToString(), userList[i].Username).WithColor(Color.Blue);
            }
            await ReplyAsync("", false, builder.Build());
        }

        [Command("Help")]
        public async Task ShowHelp()
        {
            builder.AddField("Bet Bot Help", "~bet Balance: Return current player balance\n" + 
                "~bet Freecoin: Get free coins or initiate account. Free coins every hour\n" + 
                "~bet Hi 1: Bet high with amount of 1 coin(Amount can be changed accordingly)\n" +
                "~bet Lo 1: Bet high with amount of 1 coin(Amount can be changed accordingly)\n" + 
                "~bet Top5: List Top5 players who has the most coins" + 
                "~bet About: Show information about this bot" + 
                "~bet Help: Show this document");
            await ReplyAsync("", false, builder.Build());
        }

        [Command("About")]
        public async Task About()
        {
            builder.AddField("About", "GameMode\nBet high or low.\n\n" +
                "Win condition\n" +
                "High: Roll number > 50.5\n" +
                "Low: Roll number < 49.5\n\n" +
                "How to add this bot to your channel\n" +
                "https://discordapp.com/oauth2/authorize?client_id=451907755448467467&scope=bot&permissions=3072" + "\n\n" +
                "Please play legally...\n");
            await ReplyAsync("", false, builder.Build());

        }
    }
}
